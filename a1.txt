1. Create course_enrollment database
-----------------------------------------------------

1. a.MongoDB Atlas>Right Click>Create Database>Input DB name "Enrollment">Create
   
   b.Under DB "Enrollment">Open New Shell>db.createCollection("course_enrollment")


#####################################################
2. Create the following schema for users and courses

users = {
    firstName,
    lastName,
    email,
    password,
    isAdmin,
    mobileNumber,
    isActive
    enrollments: [
        {
            coursesId,
        }
    ]
}

courses = {
    name,
    description,
    status,
    isActive,
    createdOn,
    enrollees: [
        {
            usersId,
        }
    ]
}
-------------------------------------------------------

2.a.
var users = ObjectId()
db.course_enrollment.insert(
{
users : {
    firstName : "Diane",
    lastName : "Murphy",
    email : "dmurphy@mail.com",
    password : "*******",
    isAdmin : false,
    mobileNumber : "09xxxxxxxx",
    isActive : true,
    enrollments: [""]
}})

b.
db.course_enrollment.insert(
{
users : {
    firstName : "Mary",
    lastName : "Patterson",
    email : "mpatterson@mail.com",
    password : "*******",
    isAdmin : false,
    mobileNumber : "09xxxxxxxx",
    isActive : true,
    enrollments: [""]
}})
c.
db.course_enrollment.insert(
{
users : {
    firstName : "Jeff",
    lastName : "Firrelli",
    email : "jfirrelli@mail.com",
    password : "*******",
    isAdmin : false,
    mobileNumber : "09xxxxxxxx",
    isActive : true,
    enrollments: [""]
}})
d.
db.course_enrollment.insert(
{
users : {
    firstName : "Gerard",
    lastName : "Bondur",
    email : "gbondur@mail.com",
    password : "*******",
    isAdmin : false,
    mobileNumber : "09xxxxxxxx",
    isActive : true,
    enrollments: [""]
}})
e.
db.course_enrollment.insert(
{
users : {
    firstName : "Pamela",
    lastName : "Castillo",
    email : "pcastillo@mail.com",
    password : "*******",
    isAdmin : true,
    mobileNumber : "09xxxxxxxx",
    isActive : false,
    enrollments: [""]
}})
f.
db.course_enrollment.insert(
{
users : {
    firstName : "George",
    lastName : "Valuf",
    email : "gvaluf@mail.com",
    password : "*******",
    isAdmin : true,
    mobileNumber : "09xxxxxxxx",
    isActive : true,
    enrollments: [""]
}})

#######################################################

3. Add the following users with users ID:

	ID	    First Name	Last Name	Email			Is Admin?	Is Active?
	users1	Diane 		Murphy		dmurphy@mail.com	no		yes
	users2	Mary		Patterson	mpatterson@mail.com	no		yes
	users3	Jeff		Firrelli	jfirrelli@mail.com	no		yes
	users4	Gerard		Bondur		gbondur@mail.com	no		yes
	users5	Pamela		Castillo	pcastillo@mail.com	yes		no
	users6	George		Vanauf		gvanauf@mail.com	yes		yes
-------------------------------------------------------

3.
a.
var users = ObjectId()
db.course_enrollment.insert(
{
users : {
    firstName : "Diane",
    lastName : "Murphy",
    email : "dmurphy@mail.com",
    password : "*******",
    isAdmin : false,
    mobileNumber : "09xxxxxxxx",
    isActive : true,
    enrollments: [""]
}})

b.
db.course_enrollment.insert(
{
users : {
    firstName : "Mary",
    lastName : "Patterson",
    email : "mpatterson@mail.com",
    password : "*******",
    isAdmin : false,
    mobileNumber : "09xxxxxxxx",
    isActive : true,
    enrollments: [""]
}})
c.
db.course_enrollment.insert(
{
users : {
    firstName : "Jeff",
    lastName : "Firrelli",
    email : "jfirrelli@mail.com",
    password : "*******",
    isAdmin : false,
    mobileNumber : "09xxxxxxxx",
    isActive : true,
    enrollments: [""]
}})
d.
db.course_enrollment.insert(
{
users : {
    firstName : "Gerard",
    lastName : "Bondur",
    email : "gbondur@mail.com",
    password : "*******",
    isAdmin : false,
    mobileNumber : "09xxxxxxxx",
    isActive : true,
    enrollments: [""]
}})
e.
db.course_enrollment.insert(
{
users : {
    firstName : "Pamela",
    lastName : "Castillo",
    email : "pcastillo@mail.com",
    password : "*******",
    isAdmin : true,
    mobileNumber : "09xxxxxxxx",
    isActive : false,
    enrollments: [""]
}})
f.
db.course_enrollment.insert(
{
users : {
    firstName : "George",
    lastName : "Valuf",
    email : "gvaluf@mail.com",
    password : "*******",
    isAdmin : true,
    mobileNumber : "09xxxxxxxx",
    isActive : true,
    enrollments: [""]
}})

#######################################################

4. Create a variable "course1" that will store a newly generated objectId
-------------------------------------------------------

4.
a.db.createCollection("courses")

b. var course = ObjectId()

#######################################################

5. Create a course and provide the user id

	ID	    Name	Description			        Price	 	isActive	createdOn		
	course1	PD	    Professional Development	10 000		Yes		Feb. 2, 2021	
	course2	BP	    Business Processing		    13 000		Yes		Feb. 10, 2021	
	course3	FD	    Front End Development		15 000		No		Jan. 5, 2021	
-------------------------------------------------------

5.
a.
db.courses.insert(
{
course : {
    name : "PD",
    description:"Professional Developement",
    price : 10000,
    isActive: true,
    createdOn: 02/02/2021,
    enrollees: [""]
}})

b.
db.courses.insert(
{
course : {
    name : "BP",
    description:"Business Processing",
    price : 13000,
    isActive: true,
    createdOn: 02/10/2021,
    enrollees: [""]
}})

c.db.courses.insert(
{
course : {
    name :"FD",
    description:"Front End Development",
    price : 15000,
    isActive: false,
    createdOn: 01/05/2021,
    enrollees: [""]
}})

#######################################################

6. Get users IDs and add them as enrollees of the courses (update):
	Name	Enrollees
	PD	    Murphy, Firelli, Castillo
	BP	    Patterson, Bondur, Murphy
	FD	    Castillo, Vanauf
-------------------------------------------------------

6.
a.
db.course_enrollment.updateOne(
{"_id": ObjectId("6054516342cdcbebf30c185a")},
{ $set: {"enrollments": ["PD",  "BP"]}})

b.
db.course_enrollment.updateOne(
{"_id": ObjectId("6054517042cdcbebf30c185b")},
{ $set: {"enrollments": ["BP"]}})

c.
db.course_enrollment.updateOne(
{"_id": ObjectId("6054518042cdcbebf30c185c")},
{ $set: {"enrollments": ["PD"]}})

d.
db.course_enrollment.updateOne(
{"_id": ObjectId("6054518a42cdcbebf30c185d")},
{ $set: {"enrollments": ["BP"]}})

e.
db.course_enrollment.updateOne(
{"_id": ObjectId("6054519542cdcbebf30c185e")},
{ $set: {"enrollments": ["PD","FD"]}})

f.
db.course_enrollment.updateOne(
{"_id": ObjectId("6054519e42cdcbebf30c185f")},
{ $set: {"enrollments": ["FD"]}})


#######################################################

7. Get the names of the users who are not administrators
-------------------------------------------------------

7.
a.
db.course_enrollment.find(
{"users.isAdmin": false})


#######################################################

8. Aggregate through the users collection using field projection and sorting
-------------------------------------------------------

8.
a.
db.course_enrollment.aggregate([
   {  $match: {"users.isAdmin": false }}, 
   {  $group: {_id: "$users" , total: {$sum: "$users"}  } }
   ]);

#######################################################

9. Use the count operator to count the total number of users on isActive
-------------------------------------------------------

9.db.course_enrollment.aggregate([
   {  $match: {"users.isActive": true }}, 
   {  $group: {_id: "$users" , total: {$sum: "$users"}  } }
   ]);

#######################################################

10. Show students who are not active in their course, without showing the course IDs. 

-------------------------------------------------------

10.
a.
db.course_enrollment.find(
{"users.isActive": false})

#######################################################

Each bullet must have their own aggregation pipeline code. 
Save the code in a file (s19a1_<lastname>.txt) and push it to your GitLab repository. 
Update your tracker and your Boodle Account.